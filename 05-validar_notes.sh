#! /bin/bash
#@Roberto Martínez - ASIX - M01
#Programa que ens diu si estem aprovats o suspesos. 

# mirar si el número d'arguments no és correcte
if [ $# -ne 1 ]
then
  echo "ERROR: El número d'arguments no és correcte"
  echo "Usage: prog arg1"
  exit 1
fi

nota=$1

# mirar si el valor introduït és una nota
if [ $1 -lt 0 -o $1 -gt 10 ]
  echo "ERROR: La nota no és vàlida"
  echo "La nota ha de tenir un valor entre 0 i 10"
  exit 2
fi

# mirar si la nota és aprovat o suspès
if [ $1 -lt 5 ]
then
  echo "Suspès"
  exit 3

else  
  echo "Aprovat"
fi
exit 4
