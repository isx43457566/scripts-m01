#!/ /bin/bash
# @Roberto Martinez ASIX-M01
# Validar que hi ha dos arguments i mostrar-los
# ----------------------------------------------
# si el número d'arguments no és correcte informar d'error i sortir
if [ $# -ne 2 ]
then
  echo "ERROR: Número d'arguments incorrecte"
  echo "usage: prog arg1 arg2"
  exit 1
fi
# mostrar xixa
echo "Els dos arguments són: $1, $2 "
exit 2

